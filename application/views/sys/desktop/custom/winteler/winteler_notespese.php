
 
    <script type="text/javascript">

       $(document).ready(function(){
            $('select').formSelect();
    
    
        });
  
    function salva_notespese(el)
    {
        var tipo=$('#tipo').val();
        var importo=$('#importo').val();
        var pagamento=$('#pagamento').val();
        if((tipo!='')&&(tipo!=null)&&(importo!='')&&(importo!=null)&&(pagamento!='')&&(pagamento!=null))
        {
            
        
            var fd = new FormData();
            var files = $('#file-upload')[0].files;
            $.each(files, function(i, file) {
                fd.append('file[]', file);
            });
            fd.append('tipo',$('#tipo').val());
            fd.append('importo',$('#importo').val());
            fd.append('pagamento',$('#pagamento').val());
            fd.append('note',$('#note').val());
            $.ajax({
                url: '<?= controller_url()."winteler_salva_notespese" ?>',
                method: 'POST',
                data: fd,
                cache: false,
                contentType: false,
                processData: false,
                success:function(response){

                  //$('html').html(response);
                  winteler_load_conferma_salvataggio(el);
                },
                error:function(){
                    alert('errore');
                }
            });
        }
        else
        {
            alert('compilare i campi');
        }
        
    }
        
        
    </script>

    <div class="row">
        <div class="input-field col s12">
            <select id="tipo">
              <option value="" disabled selected>Tipo</option>
              <option value="Carburante">Carburante</option>
              <option value="Ristorazione">Ristorazione</option>
              <option value="Omaggi clienti">Omaggi clienti</option>
              <option value="Altro">Altro</option>
            </select>
            <label>Materialize Select</label>
        </div>
        <div class="input-field col s12">
            <input id="importo" type="number" class="validate">
            <label for="importo">Importo</label>
        </div>
        <div class="input-field col s12">
            <select id="pagamento">
              <option value="" disabled selected>Pagamento</option>
              <option value="Carta di Credito Garage"> Carta di Credito Garage</option>
              <option value="Carta di Credito Propria">Carta di Credito Propria</option>
              <option value="Contanti da rimborsare">Contanti da rimborsare</option>
            </select>
            <label>Materialize Select</label>
        </div>
        <div class="input-field col s12">
            <textarea id="note" class="materialize-textarea"></textarea>
            <label for="note">Note</label>
        </div>
        <div class="input-field col s12">
            <div class="btn">
                <label for="file-upload" class="custom-file-upload">
                    <i class="fa fa-cloud-upload"></i> Carica foto
                </label>
                <input id="file-upload" type="file" accept="image/*" capture multiple/>
            </div>

        </div>

        <div class="input-field col s12" >
            <a style="width: 100%" class="waves-effect waves-light btn" onclick="salva_notespese(this)">Salva</a>
        </div>
        <div class="row" >
            <div class='btn' onclick="winteler_load_menu(this)">Menu</div>
        </div>
    </div>
                    