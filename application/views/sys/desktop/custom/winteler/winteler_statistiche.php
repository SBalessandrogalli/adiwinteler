<script type="text/javascript">
    
    
    
    $(document).ready(function(){
            $('select').formSelect();
            $('.datepicker').datepicker({
                format: 'yyyymmdd'
            });
    
        });
        
    function winteler_load_statistiche_filtrate(el)
    {
        var fd = new FormData();
        fd.append('datadal',$('#datadal').val());
        fd.append('dataal',$('#dataal').val());
        fd.append('venditore',$('#venditore').val());
        $.ajax({
            url: '<?= controller_url()."winteler_load_statistiche" ?>',
            method: 'POST',
            data: fd,
            cache: false,
            contentType: false,
            processData: false,
            success:function(response){
              $('.card-content').html(response);
            },
            error:function(){
                alert('errore');
            }
        });
    }
    
    
    
</script>

<div class="row">
    <div class="row" >
        <div class="row">
            <div class="input-field col s6">
                <input id="datadal" type="text" class="validate field datepicker" style="color:#9e9e9e" name="data" >
                <label class=""  for="datadal">Dal</label>
            </div>
            <div class="input-field col s6">
                <input id="dataal" type="text" class="validate field datepicker" style="color:#9e9e9e" name="data" >
                <label class=""  for="dataal">Al</label>
            </div>
        </div>
        
        <div class="row">
                        <div class="input-field col s12">
                            <select id="venditore" class="field" name="venditore">
                                <option value="" disabled selected></option>
                                <?php
                                    foreach ($venditori as $key => $venditore) {
                                        ?>
                                        <option value="<?=$venditore['FIUS']?>"><?=$venditore['FSGL']?></option>
                                    <?php
                                    }
                                ?>
                            </select>
                            <label>Venditore</label>
                        </div>
                        
            </div>
        <div class="row">
            <div class="col s4">
                <div class='btn' onclick="winteler_load_statistiche_filtrate(this)">Filtra</div>
            </div>
        </div>
        <?php
        $labels="";
        $data="";
        foreach ($rows as $key => $row) {
            $user=$row['user'];
            $username=$user['FSGL'];
            $counter=$row['counter'];
            
            if($labels!='')
            {
                $labels=$labels.",";
            }
            $labels=$labels."'$username'";
            
            if($data!='')
            {
                $data=$data.",";
            }
            $data=$data."$counter";
            
            
        ?>
        <br/><br/>
        
        <div style="text-align: left;">
            <?=$username?>:<?=$counter?>
        </div>
        <?php
        }
        ?>
        <br/><br/>
        <canvas id="myChart" width="400" height="400"></canvas>
        <script>
        if(myChart!=null){
            myChart.destroy();
        }   
        var ctx = document.getElementById('myChart').getContext('2d');
        myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [<?=$labels?>],
                datasets: [{
                    label: '',
                    data: [<?=$data?>],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
        </script>
        
        
    </div>
    <div class="row" >
        <div class='btn' onclick="winteler_load_menu(this)">Menu</div>
    </div>
</div>